// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALTANK_TankAI_Controller_generated_h
#error "TankAI_Controller.generated.h already included, missing '#pragma once' in TankAI_Controller.h"
#endif
#define UNREALTANK_TankAI_Controller_generated_h

#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_SPARSE_DATA
#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_RPC_WRAPPERS
#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATankAI_Controller(); \
	friend struct Z_Construct_UClass_ATankAI_Controller_Statics; \
public: \
	DECLARE_CLASS(ATankAI_Controller, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(ATankAI_Controller)


#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_INCLASS \
private: \
	static void StaticRegisterNativesATankAI_Controller(); \
	friend struct Z_Construct_UClass_ATankAI_Controller_Statics; \
public: \
	DECLARE_CLASS(ATankAI_Controller, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(ATankAI_Controller)


#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankAI_Controller(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankAI_Controller) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankAI_Controller); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankAI_Controller); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankAI_Controller(ATankAI_Controller&&); \
	NO_API ATankAI_Controller(const ATankAI_Controller&); \
public:


#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankAI_Controller(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankAI_Controller(ATankAI_Controller&&); \
	NO_API ATankAI_Controller(const ATankAI_Controller&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankAI_Controller); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankAI_Controller); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankAI_Controller)


#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_PRIVATE_PROPERTY_OFFSET
#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_10_PROLOG
#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_RPC_WRAPPERS \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_INCLASS \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_INCLASS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALTANK_API UClass* StaticClass<class ATankAI_Controller>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealTank_Source_UnrealTank_Public_TankAI_Controller_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealTank/Public/TankAI_Controller.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTankAI_Controller() {}
// Cross Module References
	UNREALTANK_API UClass* Z_Construct_UClass_ATankAI_Controller_NoRegister();
	UNREALTANK_API UClass* Z_Construct_UClass_ATankAI_Controller();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_UnrealTank();
// End Cross Module References
	void ATankAI_Controller::StaticRegisterNativesATankAI_Controller()
	{
	}
	UClass* Z_Construct_UClass_ATankAI_Controller_NoRegister()
	{
		return ATankAI_Controller::StaticClass();
	}
	struct Z_Construct_UClass_ATankAI_Controller_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATankAI_Controller_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealTank,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATankAI_Controller_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TankAI_Controller.h" },
		{ "ModuleRelativePath", "Public/TankAI_Controller.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATankAI_Controller_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATankAI_Controller>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATankAI_Controller_Statics::ClassParams = {
		&ATankAI_Controller::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATankAI_Controller_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATankAI_Controller_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATankAI_Controller()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATankAI_Controller_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATankAI_Controller, 4027534758);
	template<> UNREALTANK_API UClass* StaticClass<ATankAI_Controller>()
	{
		return ATankAI_Controller::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATankAI_Controller(Z_Construct_UClass_ATankAI_Controller, &ATankAI_Controller::StaticClass, TEXT("/Script/UnrealTank"), TEXT("ATankAI_Controller"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATankAI_Controller);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

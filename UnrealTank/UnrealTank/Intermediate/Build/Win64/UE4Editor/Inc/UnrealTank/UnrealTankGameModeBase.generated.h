// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALTANK_UnrealTankGameModeBase_generated_h
#error "UnrealTankGameModeBase.generated.h already included, missing '#pragma once' in UnrealTankGameModeBase.h"
#endif
#define UNREALTANK_UnrealTankGameModeBase_generated_h

#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_SPARSE_DATA
#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_RPC_WRAPPERS
#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealTankGameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealTankGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealTankGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(AUnrealTankGameModeBase)


#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealTankGameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealTankGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealTankGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(AUnrealTankGameModeBase)


#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealTankGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealTankGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealTankGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealTankGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealTankGameModeBase(AUnrealTankGameModeBase&&); \
	NO_API AUnrealTankGameModeBase(const AUnrealTankGameModeBase&); \
public:


#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealTankGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealTankGameModeBase(AUnrealTankGameModeBase&&); \
	NO_API AUnrealTankGameModeBase(const AUnrealTankGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealTankGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealTankGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealTankGameModeBase)


#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_12_PROLOG
#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_RPC_WRAPPERS \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_INCLASS \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALTANK_API UClass* StaticClass<class AUnrealTankGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealTank_Source_UnrealTank_UnrealTankGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

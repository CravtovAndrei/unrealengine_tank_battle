// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALTANK_TankParentController_BP_generated_h
#error "TankParentController_BP.generated.h already included, missing '#pragma once' in TankParentController_BP.h"
#endif
#define UNREALTANK_TankParentController_BP_generated_h

#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_SPARSE_DATA
#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_RPC_WRAPPERS
#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATankParentController_BP(); \
	friend struct Z_Construct_UClass_ATankParentController_BP_Statics; \
public: \
	DECLARE_CLASS(ATankParentController_BP, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(ATankParentController_BP)


#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_INCLASS \
private: \
	static void StaticRegisterNativesATankParentController_BP(); \
	friend struct Z_Construct_UClass_ATankParentController_BP_Statics; \
public: \
	DECLARE_CLASS(ATankParentController_BP, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(ATankParentController_BP)


#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankParentController_BP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankParentController_BP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankParentController_BP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankParentController_BP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankParentController_BP(ATankParentController_BP&&); \
	NO_API ATankParentController_BP(const ATankParentController_BP&); \
public:


#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATankParentController_BP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATankParentController_BP(ATankParentController_BP&&); \
	NO_API ATankParentController_BP(const ATankParentController_BP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATankParentController_BP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATankParentController_BP); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATankParentController_BP)


#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_PRIVATE_PROPERTY_OFFSET
#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_13_PROLOG
#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_RPC_WRAPPERS \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_INCLASS \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_INCLASS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_TankParentController_BP_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALTANK_API UClass* StaticClass<class ATankParentController_BP>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealTank_Source_UnrealTank_TankParentController_BP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

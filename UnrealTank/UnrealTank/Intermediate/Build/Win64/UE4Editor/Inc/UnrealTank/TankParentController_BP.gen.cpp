// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealTank/TankParentController_BP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTankParentController_BP() {}
// Cross Module References
	UNREALTANK_API UClass* Z_Construct_UClass_ATankParentController_BP_NoRegister();
	UNREALTANK_API UClass* Z_Construct_UClass_ATankParentController_BP();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_UnrealTank();
// End Cross Module References
	void ATankParentController_BP::StaticRegisterNativesATankParentController_BP()
	{
	}
	UClass* Z_Construct_UClass_ATankParentController_BP_NoRegister()
	{
		return ATankParentController_BP::StaticClass();
	}
	struct Z_Construct_UClass_ATankParentController_BP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATankParentController_BP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealTank,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATankParentController_BP_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TankParentController_BP.h" },
		{ "ModuleRelativePath", "TankParentController_BP.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATankParentController_BP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATankParentController_BP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATankParentController_BP_Statics::ClassParams = {
		&ATankParentController_BP::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATankParentController_BP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATankParentController_BP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATankParentController_BP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATankParentController_BP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATankParentController_BP, 2277508727);
	template<> UNREALTANK_API UClass* StaticClass<ATankParentController_BP>()
	{
		return ATankParentController_BP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATankParentController_BP(Z_Construct_UClass_ATankParentController_BP, &ATankParentController_BP::StaticClass, TEXT("/Script/UnrealTank"), TEXT("ATankParentController_BP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATankParentController_BP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

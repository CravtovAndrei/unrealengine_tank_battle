// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALTANK_AITank_Controller_generated_h
#error "AITank_Controller.generated.h already included, missing '#pragma once' in AITank_Controller.h"
#endif
#define UNREALTANK_AITank_Controller_generated_h

#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_SPARSE_DATA
#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_RPC_WRAPPERS
#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAITank_Controller(); \
	friend struct Z_Construct_UClass_AAITank_Controller_Statics; \
public: \
	DECLARE_CLASS(AAITank_Controller, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(AAITank_Controller)


#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAITank_Controller(); \
	friend struct Z_Construct_UClass_AAITank_Controller_Statics; \
public: \
	DECLARE_CLASS(AAITank_Controller, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealTank"), NO_API) \
	DECLARE_SERIALIZER(AAITank_Controller)


#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAITank_Controller(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAITank_Controller) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAITank_Controller); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAITank_Controller); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAITank_Controller(AAITank_Controller&&); \
	NO_API AAITank_Controller(const AAITank_Controller&); \
public:


#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAITank_Controller(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAITank_Controller(AAITank_Controller&&); \
	NO_API AAITank_Controller(const AAITank_Controller&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAITank_Controller); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAITank_Controller); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAITank_Controller)


#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_12_PROLOG
#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_RPC_WRAPPERS \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_INCLASS \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_SPARSE_DATA \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_INCLASS_NO_PURE_DECLS \
	UnrealTank_Source_UnrealTank_Public_AITank_Controller_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALTANK_API UClass* StaticClass<class AAITank_Controller>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealTank_Source_UnrealTank_Public_AITank_Controller_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

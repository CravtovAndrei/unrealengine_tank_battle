// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAI_Controller.h"

ATank* ATankAI_Controller::GetControllerTank() const
{
    return Cast<ATank>(GetPawn());
}

void ATankAI_Controller::BeginPlay()
{
    Super::BeginPlay();
    UE_LOG(LogTemp, Warning, TEXT("AI TANK IS - %s"), *GetControllerTank()->GetName());
}

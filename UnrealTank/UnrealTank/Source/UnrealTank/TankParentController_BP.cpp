// Fill out your copyright notice in the Description page of Project Settings.

#include "TankParentController_BP.h"

ATank* ATankParentController_BP::GetControllerTank() const
{
    return Cast<ATank>(GetPawn());
}

void ATankParentController_BP::BeginPlay()
{
    Super::BeginPlay();

    UE_LOG(LogTemp, Warning, TEXT("PLAYER TANK IS - %s"), *GetControllerTank()->GetName());
    
}

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealTankGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALTANK_API AUnrealTankGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

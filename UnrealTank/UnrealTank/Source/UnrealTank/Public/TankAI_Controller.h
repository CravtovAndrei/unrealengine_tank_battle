// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Tank.h"
#include "TankAI_Controller.generated.h"

UCLASS()
class UNREALTANK_API ATankAI_Controller : public AAIController
{
	GENERATED_BODY()

public:
	ATank* GetControllerTank() const;
	void BeginPlay() override;
	
};

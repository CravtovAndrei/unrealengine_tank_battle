// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Tank.h"
#include "TankParentController_BP.generated.h"

/**
 * 
 */
UCLASS()
class UNREALTANK_API ATankParentController_BP : public APlayerController
{
	GENERATED_BODY()

public:
	ATank* GetControllerTank() const;

	void BeginPlay() override;

	
	
};

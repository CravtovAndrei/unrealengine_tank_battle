// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealTank.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealTank, "UnrealTank" );
